# slackware-service
A simple wrapper around Slackware RC scripts

## Setup

You'll probably want to alias this in /root/.bashrc or /root/.profile

    alias service='/root/bin/slackware-service.pl'      # this may collide with scripts from other distros

  or

    alias slkservice='/root/bin/slackware-service.pl'   # safer, more traditional slackware naming convention

## Usage

    root@cowabongos:~# ps auxw | grep named
    root      3991  0.0  0.0   2748   776 pts/22   S+   20:18   0:00 grep named
    root@cowabongos:~# service bind start
    Starting BIND:  /usr/sbin/named
    root@cowabongos:~# ps auxw | grep named
    root      3995  1.6  0.5  54380 14540 ?        Ssl  20:18   0:00 /usr/sbin/named
    root      4004  0.0  0.0   2748   776 pts/22   S+   20:18   0:00 grep named

    Available actions: start stop restart status

    To see all available enabled user services in /etc/rc.d/, run "./slackware-service.pl show"

    To see all available enabled user *and* system services in /etc/rc.d/, run "./slackware-service.pl showall"

## Note

As noted in the script itself, we treat essential system scripts as reserved and decline to manage them here. The scripts can be still run manually, of course.

    To see the reserved list, run "./slackware-service.pl showreserved".

The system administrator is welcome to edit the lists as he sees fit.
