#!/usr/bin/perl

# File: slackwareservice.pl
# Copyright Joey Kelly (joeykelly.net)
# November 28, 2017
# License: GPL version 2

# This is a simple wrapper around the various Slackware RC scripts.
# See the GitHub README for usage: https://github.com/mmlj4/slackware-service

use strict;

#use warnings;
# we don't want any "experimental" smartmatch warnings, so...
no warnings;

use feature qw(switch say);

my $rc = '/etc/rc.d/rc.';
my @actions = qw(start stop restart status graceful graceful-stop reload force-reload);

# what service do we want to manage?
my $service = shift;
$service = 'help' unless defined $service;
$service = safechars($service);



# list available services

# running these makes no sense... you wouldn't want to restart a system shutdown (rc.6), would you?
my @reserved = qw(
  rc.4
  rc.6
  rc.K
  rc.M
  rc.S
  rc.acpid
  rc.cgmanager
  rc.cgproxy
  rc.cpufreq
  rc.font
  rc.inet2
  rc.keymap
  rc.local
  rc.loop
  rc.mcelog
  rc.modules
  rc.modules.local
  rc.local_shutdown
  rc.syslog
  rc.sysvinit
  rc.udev
  rc.ulogd
);
my %reserved;
foreach (@reserved) {$reserved{$_}++; }

# we normally hide these system services from view. If you want to see them all, issue "service showall"
my @system = qw(
  rc.bluetooth
  rc.consolekit
  rc.fuse
  rc.inet1
  rc.inetd
  rc.loop
  rc.syslog
  rc.ulogd
  rc.wireless
);
my %system;
foreach (@system) {$system{$_}++; }



# help
my $actions = join ' ', @actions;
my $help = <<"HELP";
slackware-service.pl example usage:

root\@cowabongos:~# ps auxw | grep named
root      3991  0.0  0.0   2748   776 pts/22   S+   20:18   0:00 grep named
root\@cowabongos:~# service bind start
Starting BIND:  /usr/sbin/named
root\@cowabongos:~# ps auxw | grep named
root      3995  1.6  0.5  54380 14540 ?        Ssl  20:18   0:00 /usr/sbin/named
root      4004  0.0  0.0   2748   776 pts/22   S+   20:18   0:00 grep named

Available actions: $actions

To see all available enabled user services in /etc/rc.d/, run "./slackware-service.pl show"

To see all available enabled user *and* system services in /etc/rc.d/, run "./slackware-service.pl showall"

*NOTE:* This script deliberately declines to manage certain *critical* system services. To see the reserved list, run "./slackware-service.pl showreserved"

For convenience, you'll probably want to alias this in /root/.bashrc or /root/.profile:
alias slkservice='/root/bin/slackware-service.pl'
HELP

if ($service eq 'help') {
  say $help;
  exit;
}

if ($service eq 'show') {
  my @servicelist = `find /etc/rc.d/ -executable -type f -maxdepth 1 | sort`;
  foreach (@servicelist) {
    chomp;
    $_ =~ s|/etc/rc.d/||g;
    unless ($reserved{$_} or $system{$_}) {
      $_=~ s/^rc\.//;
      say;
    }
  }
  exit;
}

if ($service eq 'showall') {
  my @servicelist = `find /etc/rc.d/ -executable -type f -maxdepth 1 | sort`;
  foreach (@servicelist) {
    chomp;
    $_ =~ s|/etc/rc.d/||g;
    unless ($reserved{$_}) {
      $_=~ s/^rc\.//;
      say;
    }
  }
  exit;
}

if ($service eq 'showreserved') {
  foreach (sort keys %reserved) {
    say;
  }
  exit;
}



# make the donuts
my $daemon = $rc . $service;
if (-x $daemon) {
  # we have a valid service, so what do we want to do with it?
  my $action = shift;
  $action = safechars($action);
  if ($action ~~ @actions) {
    # we have a valid service and a plausible action, so let's do it
    if ($reserved{"rc.".$service}) {
      say "$service is a reserved system service. If you really need to run this RC script, please do so manually.";
    } else {
      # we want to vacate any shell environment variables, etc., so we use "env -i"
      system "env -i $daemon $action";
    }
  } else {
    say "$action is not a valid action";
  }
} else {
  say "$service is not a valid service";
}



# let's sanitize user input, like a good programmer should...
sub safechars {
  my $string = shift;
  $string =~ tr/a-zA-Z0-9-//dc;
  return $string;
}
